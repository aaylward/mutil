Installation
============

.. role:: zsh(code)
   :language: zsh

With pip
--------

To install :zsh:`mutil` in an existing environment:

.. code-block:: zsh

   pip install mutil

In a conda environment
----------------------

To install :zsh:`mutil` in a conda environment:

.. code-block:: zsh

   conda create -c conda-forge -n mutil psutil
   conda activate mutil
   pip install mutil

Alternatively, you may wish to include :zsh:`mutil` and :zsh:`nvtop` in one environment:

.. code-block:: zsh

   conda create -c conda-forge -n mutil-nvtop psutil nvtop
   conda activate mutil-nvtop
   pip install mutil

Check installation
------------------

Check that the installation was successful by running any of:

.. code-block:: none

   mutil --version
   mutil --help
   mutil

See the available subcommands by running :zsh:`mutil --help` or simply :zsh:`mutil`:

.. code-block:: none

   usage: mutil [-h] [--version]
             {current,previous,proposed,draft,enact,free,induct,repeal,reserve,retire,usage,update-shared}
             ...

   Display the current memory policy

   positional arguments:
     {current,previous,proposed,draft,enact,free,induct,repeal,reserve,retire,usage,update-shared}
       current             display the current memory policy
       previous            display the previous memory policy
       proposed            display the proposed memory policy
       draft               draft a memory policy proposal
       enact               enact the proposed memory policy
       free                free up memory in the proposed policy
       induct              induct a new user into the memory policy
       repeal              repeal the last enacted memory policy
       reserve             add a reservation to the proposed memory policy
       retire              retire a user from the memory policy
       usage               display memory usage for users or groups
       update-shared       update the shared memory value in current and proposed policies

   options:
     -h, --help            show this help message and exit
     --version             show program's version number and exit
