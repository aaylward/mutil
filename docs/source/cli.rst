CLI reference
=============

.. role:: zsh(code)
   :language: zsh

induct
------

   usage: mutil induct [-h] [-u USERS]

   options:
     -h, --help            show this help message and exit
     -u USERS, --users USERS
                           comma-separated list of users to induct

retire
------

   usage: mutil retire [-h] -u USERS

   options:
     -h, --help            show this help message and exit
     -u USERS, --users USERS
                           comma-separated list of users to retire

current
-------

   usage: mutil current [-h]

   options:
     -h, --help  show this help message and exit

previous
--------

   usage: mutil previous [-h]

   options:
     -h, --help  show this help message and exit

proposed
--------

   usage: mutil proposed [-h]

   options:
     -h, --help  show this help message and exit


draft
-----

   usage: mutil draft [-h] draft

   positional arguments:
     draft

   options:
     -h, --help  show this help message and exit

reserve
-------

   usage: mutil reserve [-h] [-g GROUP] [-u USERS] memory_in_gigabytes

   positional arguments:
     memory_in_gigabytes

   options:
     -h, --help            show this help message and exit
     -g GROUP, --group GROUP
                           group name for memory reservation
     -u USERS, --users USERS
                           comma-separated list of usernames for memory reservation

free
----

   usage: mutil free [-h] [-g GROUP] [-u USERS]

   options:
     -h, --help            show this help message and exit
     -g GROUP, --group GROUP
                           group to free memory from
     -u USERS, --users USERS
                           comma-separated list of users to free memory from

enact
-----

   usage: mutil enact [-h] [--no-daemon]

   options:
     -h, --help   show this help message and exit
     --no-daemon  Don't restart the daemon. (This is for use in setup).

repeal
------

   usage: mutil repeal [-h]

   options:
     -h, --help  show this help message and exit

usage
-----

   usage: mutil usage [-h] [-g GROUPS] [-u USERS] [-k | -m]

   options:
     -h, --help            show this help message and exit
     -g GROUPS, --groups GROUPS
                           comma-separated list of groups for usage display
     -u USERS, --users USERS
                           comma-separated list of usernames for usage display
     -k, --kilobytes       display usage in kilobytes
     -m, --megabytes       display usage in megabytes

update-shared
-------------

   usage: mutil update-shared [-h] [--shared SHARED]

   options:
     -h, --help       show this help message and exit
     --shared SHARED  impose a shared memory value for testing
