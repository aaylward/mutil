Tutorial
========

.. role:: zsh(code)
   :language: zsh

Induct a user
-------------

Each user must be *inducted* into the memory policy in order to be tracked and
controlled. To induct a user:

.. code-block:: zsh

   mutil induct -u <username>

Then the user will be included in the proposed policy

.. code-block:: zsh

   mutil proposed

After enacting the proposed policy, the new user will be included in the
current policy

.. code-block:: zsh

   sudo `which mutil` enact
   mutil current


Monitoring memory usage
-----------------------

Check your memory usage

.. code-block:: zsh

   mutil usage

Check usage of another user

.. code-block:: zsh

   mutil usage -u <username>

Check usage of a group

.. code-block:: zsh

   mutil usage -g free

Reserve memory
--------------

.. code-block:: zsh

   mutil reserve <memory in gigabytes>

This updates the proposed policy

.. code-block:: zsh

   mutil proposed

For the reservation to take effect, enact the proposed policy

.. code-block:: zsh

   sudo `which mutil` enact
   mutil current

Retire a user
-------------

After a user has been removed from the system, they should be retired from the memory policy to avoid clutter.

.. code-block:: zsh

   mutil retire -u <username>
