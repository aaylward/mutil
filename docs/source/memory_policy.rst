The memory policy
=================

.. role:: zsh(code)
   :language: zsh

:zsh:`mutil` maintains a JSON file defining the currently active memory policy at :zsh:`/home/memory-policy/current-memory-policy.json`. Here is the default configuration on a machine with 1 TB RAM, viewable with :zsh:`mutil current`:

.. code-block:: json

   {
     "free": {
       "memory_limit": 993,
       "users": [
         "user1",
         "user2",
         "user3"
       ]
     },
     "shared": 0
   }

The JSON has two entries, :zsh:`"free"` and :zsh:`"shared"`. :zsh:`"free"` is the default control group, which contains all users and available RAM when no memory is reserved in other groups. Since the memory is not reserved for anyone, it is "free." :zsh:`"shared"` is a value that tracks how much memory is in use but is not assigned to a specific user.

Note that the amount of memory in the :zsh:`"free"` group is 993 GB, which is less than 1 TB. This is because a small buffer of memory is kept unused, to ensure the memory controller works reliably.

There are two other JSON files in the memory policy directory: :zsh:`proposed-memory-policy.json` and :zsh:`previous-memory-policy.json`. They each contain their own policy definitions, which are checked when a policy is enacted or repealed (see below). They can be viewed with :zsh:`mutil proposed` and :zsh:`mutil previous`.

.. code-block:: zsh

   ls /home/memory-policy/

.. code-block:: none

   current-memory-policy.json  previous-memory-policy.json  proposed-memory-policy.json

Changing the proposed memory policy
-----------------------------------

The proposed policy, contained in :zsh:`proposed-memory-policy.json`, is controlled by the :zsh:`draft`, :zsh:`free`, :zsh:`induct`, :zsh:`reserve`, and :zsh:`retire` subcommands. You are most likely to use the :zsh:`reserve` and :zsh:`free` subcommands. For example, to reserve 300 GB memory for :zsh:`user1`:

.. code-block:: zsh

   mutil reserve -u user1 300


This will update the proposed policy. For example:

.. code-block:: zsh
    mutil proposed

.. code-block:: json

   {
     "user1" {
       "memory_limit": 300,
       "users": [
         "user1"
       ]
     }
     "free": {
       "memory_limit": 693,
       "users": [
         "user2",
         "user3"
       ]
     },
     "shared": 0
   }

To free up memory that is reserved for a user in the proposed policy, use :zsh:`mutil free`:

.. code-block:: zsh

   mutil free -u user1
   mutil proposed

.. code-block:: json

   {
     "free": {
       "memory_limit": 993,
       "users": [
         "user1",
         "user2",
         "user3"
       ]
     },
     "shared": 0
   }

Enacting the proposed memory policy
-----------------------------------

For the proposed policy to take effect, it must be enacted and become the current policy. This operation is carried out by :zsh:`mutil enact`.

Note: Since :zsh:`mutil enact` requires superuser privileges to be used, in practice the command is usually: 

.. code-block:: zsh

   sudo `which mutil` enact

Repealing a memory policy
-------------------------

The opposite of enacting a policy is repealing it. When :zsh:`mutil repeal` is used, the current policy is reverted to the previous one.

Inducting and retiring users
----------------------------

New users are not automatically included in the memory policy when they are created, you will need to induct each with :zsh:`mutil induct`.

.. code-block:: zsh

   mutil induct -u user4
   mutil proposed


.. code-block:: json

   {
     "free": {
       "memory_limit": 993,
       "users": [
         "user1",
         "user2",
         "user3",
         "user4"
       ]
     },
     "shared": 0
   }

For the new user to be controlled by :zsh:`cgroups`, you will still need to :zsh:`mutil enact` so that the proposed policy becomes the current one.

If a user is removed from the system, you should retire them from the memory policy to avoid clutter:

.. code-block:: zsh

   mutil retire -u user4
   mutil proposed

.. code-block:: json

   {
     "free": {
       "memory_limit": 993,
       "users": [
         "user1",
         "user2",
         "user3"
       ]
     },
     "shared": 0
   }

Drafting a memory policy from scratch
-------------------------------------

It is possible to draft a proposed memory policy by providing a JSON string to :zsh:`mutil draft`.


.. code-block:: zsh

   mutil draft '{"user1": {"memory_limit": 100, "users": ["user1"]}, "group1": {"memory_limit": 200, "users": ["user2", "user3"]}}'
   mutil proposed

.. code-block:: json

   {
     "user1": {
       "memory_limit": 100,
       "users": [
         "user1"
       ]
     },
     "group1": {
       "memory_limit": 200,
       "users": [
         "user2",
         "user3"
       ]
     }
     "free": {
       "memory_limit": 693,
       "users": [
         "user4"
       ]
     },
     "shared": 0
   }

Update the shared memory value
------------------------------

In case the shared memory value does not automatically track the shared memory on the system, it can be updated manually by simply running :zsh:`mutil update-shared`. Running this command once in a while usually prevents any problems. You can do a test run for a given shared memory value by running :zsh:`mutil update-shared --shared <value in GB>`. For example, to test with 10 GB shared memory:

.. code-block:: zsh

   mutil update-shared --shared 10
