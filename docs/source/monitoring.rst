Monitoring memory usage
=======================

Check your memory usage

.. code-block:: zsh

   mutil usage

Check usage of another user

.. code-block:: zsh

   mutil usage -u <username>

Check usage of a group

.. code-block:: zsh

   mutil usage -g free