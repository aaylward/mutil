.. PanKmer documentation master file, created by
   sphinx-quickstart on Mon Jun  6 19:04:32 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

mutil documentation
===================

.. role:: zsh(code)
   :language: zsh

Overview
--------

A utility for managing memory usage with `cgroups <https://manpages.ubuntu.com/manpages/bionic/man7/cgroups.7.html>`_ on Ubuntu 20.04.

Memory management is an important consideration for a multi-user computing server, because if users collectively attempt to use more memory than is available on the system, it may crash. A useful means of preventing this is :zsh:`cgroups` which can assign users to "control groups" which are limited to a given quantity of resources (such as CPU, disk, or memory usage).

However, :zsh:`cgroups` are somewhat complicated to use in practice. :zsh:`mutil` streamlines the memory management component.

Contents
--------

.. toctree::

   installation
   tutorial
   memory_policy
   monitoring
   cli
