# mutil

A utility for managing memory resources with [cgroups](https://manpages.ubuntu.com/manpages/focal/man7/cgroups.7.html) on Ubuntu 20.04.

Memory management is an important consideration for a multi-user computing server, because if users collectively attempt to use more memory than is available on the system, it may crash. A useful means of preventing this is `cgroups` which can assign users to "control groups" which are limited to a given quantity of resources (such as CPU, disk, or memory usage).

However, `cgroups` are somewhat complicated to use in practice. `mutil` streamlines the memory management component.

## Installation

### With pip

To install `mutil` in an existing environment:

```sh
pip install mutil
```

### In a conda environment

To install `mutil` in a conda environment:
```sh
conda create -c conda-forge -n mutil psutil
conda activate mutil
pip install mutil
```

Alternatively, you may wish to include `mutil` and `nvtop` in one environment:
```sh
conda create -c conda-forge -n mutil-nvtop psutil nvtop
conda activate mutil-nvtop
pip install mutil
```

### Check installation

Check that the installation was successful by running any of:

```sh
mutil --version
mutil --help
mutil
```

## The memory policy

`mutil` maintains a JSON file defining the currently active memory policy at `/home/memory-policy/current-memory-policy.json`. Here is the default configuration on a machine with 1 TB RAM, viewable with `mutil current`:
```json
{
  "free": {
    "memory_limit": 993,
    "users": [
      "user1",
      "user2",
      "user3"
    ]
  },
  "shared": 0
}
```

The JSON has two entries, `"free"` and `"shared"`. `"free"` is the default control group, which contains all users and available RAM when no memory is reserved in other groups. Since the memory is not reserved for anyone, it is "free." `"shared"` is a value that tracks how much memory is in use but is not assigned to a specific user.

Note that the amount of memory in the `"free"` group is 993 GB, which is less than 1 TB. This is because a small buffer of memory is kept unused, to ensure the memory controller works reliably.

There are two other JSON files in the memory policy directory: `proposed-memory-policy.json` and `previous-memory-policy.json`. They each contain their own policy definitions, which are checked when a policy is enacted or repealed (see below). They can be viewed with `mutil proposed` and `mutil previous`.

```
ls /home/memory-policy/
```
```
current-memory-policy.json  previous-memory-policy.json  proposed-memory-policy.json
```

### Changing the proposed memory policy

The proposed policy, contained in `proposed-memory-policy.json`, is controlled by the `draft`, `free`, `induct`, `reserve`, and `retire` subcommands. You are most likely to use the `reserve` and `free` subcommands. For example, to reserve 386 GB memory for `user1`:

```sh
mutil reserve -u user1 300
```
This will update the proposed policy. For example:
```sh
mutil proposed
```
```json
{
  "user1" {
    "memory_limit": 300,
    "users": [
      "user1"
    ]
  }
  "free": {
    "memory_limit": 693,
    "users": [
      "user2",
      "user3"
    ]
  },
  "shared": 0
}
```

To free up memory that is reserved for a user in the proposed policy, use `mutil free`:
```sh
mutil free -u user1
mutil proposed
```
```json
{
  "free": {
    "memory_limit": 993,
    "users": [
      "user1",
      "user2",
      "user3"
    ]
  },
  "shared": 0
}
```

### Enacting the proposed memory policy

For the proposed policy to take effect, it must be enacted and become the current policy. This operation is carried out by `mutil enact`.

Note: Since `mutil enact` requires superuser privileges to be used, in practice the command is usually: 
```
sudo `which mutil` enact
```

### Repealing a memory policy

The opposite of enacting a policy is repealing it. When `mutil repeal` is used, the current policy is reverted to the previous one.

### Inducting and retiring users

New users are not automatically included in the memory policy when they are created, you will need to induct each with `mutil induct`.

```sh
mutil induct -u user4
mutil proposed
```
```json
{
  "free": {
    "memory_limit": 993,
    "users": [
      "user1",
      "user2",
      "user3",
      "user4"
    ]
  },
  "shared": 0
}
```

For the new user to be controlled by `cgroups`, you will still need to `mutil enact` so that the proposed policy becomes the current one.

If a user is removed from the system, you should retire them from the memory policy to avoid clutter:
```sh
mutil retire -u user4
mutil proposed
```
```json
{
  "free": {
    "memory_limit": 993,
    "users": [
      "user1",
      "user2",
      "user3"
    ]
  },
  "shared": 0
}
```

## Monitoring memory usage

Check your memory usage

```sh
mutil usage
```

Check usage of another user

```sh
mutil usage -u <username>
```

Check usage of a group

```sh
mutil usage -g free
```

## Drafting a memory policy from scratch

It is possible to draft a proposed memory policy by providing a JSON string to `mutil draft`.

```sh
mutil draft '{"user1": {"memory_limit": 100, "users": ["user1"]}, "group1": {"memory_limit": 200, "users": ["user2", "user3"]}}'
mutil proposed
```
```json
{
  "user1": {
    "memory_limit": 100,
    "users": [
      "user1"
    ]
  },
  "group1": {
    "memory_limit": 200,
    "users": [
      "user2",
      "user3"
    ]
  }
  "free": {
    "memory_limit": 693,
    "users": [
      "user4"
    ]
  },
  "shared": 0
}
```

## Update the shared memory value

In case the shared memory value does not automatically track the shared memory on the system, it can be updated manually by simply running `mutil update-shared`. Running this command once in a while usually prevents any problems. You can do a test run for a given shared memory value by running `mutil update-shared --shared <value in GB>`. For example, to test with 10 GB shared memory:
```sh
mutil update-shared --shared 10
```

